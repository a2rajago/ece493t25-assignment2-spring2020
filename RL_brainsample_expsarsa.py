import numpy as np
import pandas as pd
import random


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        """Your code goes here"""
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Expected SARSA Algorithm"

    def choose_action(self, observation):
        """Your code goes here"""
        self.check_state_exist(observation)

        if (np.random.uniform() < self.epsilon):
            action = np.random.choice(self.actions)
        else:
            state_action = self.q_table.loc[observation, :]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        return action

    def learn(self, s, a, r, s_):
        """Your code goes here"""
        self.check_state_exist(s_)

        q_current = self.q_table.loc[s, a]

        if s_ != 'terminal':
            state_action = self.q_table.loc[s_, :]
            max_actions_sum = np.sum(state_action)
            max_actions_value = np.max(state_action)
            max_actions_len = len(state_action[state_action == max_actions_value])
            max_action_prob = float((1 - self.epsilon) / max_actions_len + self.epsilon / len(self.actions))
            non_max_action_prob = float(self.epsilon / len(self.actions))
            expected_max_value = max_actions_value * max_action_prob * max_actions_len
            expected_non_max_value = (max_actions_sum - max_actions_value * max_actions_len) * non_max_action_prob

            final_expected_value = expected_max_value + expected_non_max_value
            q_predict = r + self.gamma * final_expected_value 
            a_ = self.choose_action(str(s_))
        else:
            q_predict = r
            a_ = None

        self.q_table.loc[s, a ] = q_current + self.lr * (q_predict - q_current)
        return s_, a_

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [random.randint(-1, 0) for _ in range(len(self.actions))],
                    index=self.q_table.columns,
                    name=state,
                )
            )

