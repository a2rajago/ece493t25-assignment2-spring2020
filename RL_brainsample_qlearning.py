import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        """Your code goes here"""
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Q-Learning Algorithm"

    def choose_action(self, observation):
        """Your code goes here"""
        self.check_state_exist(observation)
        
        if (np.random.uniform() < self.epsilon):
            action = np.random.choice(self.actions)
        else:
            state_action_values = self.q_table.loc[observation, :]
            action = np.random.choice(state_action_values[state_action_values == np.max(state_action_values)].index)
        return action

    def learn(self, s, a, r, s_):
        """Your code goes here"""
        self.check_state_exist(s_)

        return self.update_table_values(s, a, r, s_)

    def update_table_values(self, s, a, r, s_):
        if s_ != 'terminal':
            state_action_values = self.q_table.loc[s_, :]
            a_ = np.random.choice(state_action_values[state_action_values == np.max(state_action_values)].index)
            q_predict = r + self.gamma * self.q_table.loc[s_, a_]
        else:
            q_predict = r
            
        self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr * (q_predict - self.q_table.loc[s, a])
        return s_, self.choose_action(s_)

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
