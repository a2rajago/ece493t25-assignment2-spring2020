import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        """Your code goes here"""
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q1_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q2_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double-Q Algorithm"

    def choose_action(self, observation):
        """Your code goes here"""
        self.check_state_exist(observation)
        
        if (np.random.uniform() < self.epsilon):
            action = np.random.choice(self.actions)
        else:
            q_sum_action = self.q1_table.loc[observation, :] + self.q2_table.loc[observation, :] 
            action = np.random.choice(q_sum_action[q_sum_action == np.max(q_sum_action)].index)
        return action

    def learn(self, s, a, r, s_):
        """Your code goes here"""
        self.check_state_exist(s_)

        if np.random.random() >= 0.5:
            return self.update_table_values(self.q1_table, self.q2_table, s, a, r, s_)
        else:
            return self.update_table_values(self.q2_table, self.q1_table, s, a, r, s_)

    def update_table_values(self, q1_table, q2_table, s, a, r, s_):
        if s_ != 'terminal':
            state_action_values = q1_table.loc[s_, :]
            a_ = np.random.choice(state_action_values[state_action_values == np.max(state_action_values)].index)
            q_predict = r + self.gamma * q2_table.loc[s_, a_]
        else:
            q_predict = r
            
        q1_table.loc[s, a] = q1_table.loc[s, a] + self.lr * (q_predict - q1_table.loc[s, a])
        return s_, self.choose_action(s_)

    def check_state_exist(self, state):
        if state not in self.q1_table.index:
            # append new state to q table
            self.q1_table = self.q1_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q1_table.columns,
                    name=state,
                )
            )
        if state not in self.q2_table.index:
            # append new state to q table
            self.q2_table = self.q2_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q2_table.columns,
                    name=state,
                )
            )